'use strict';

/**
 * @ngdoc directive
 * @name contactformApp.directive:stopEvent
 * @description
 * # stopEvent
 */
angular.module('contactformApp')
    .directive('stopEvent', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                element.on(attr.stopEvent, function(e) {
                    e.stopPropagation();
                });
            }
        };
    });
