'use strict';

/**
 * @ngdoc function
 * @name contactformApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the contactformApp
 */
angular.module('contactformApp')
    .controller('MainCtrl', function($scope, Ref, $firebaseArray, $modal, $log) {
        var contacts = $firebaseArray(Ref);

        $scope.contacts = contacts;

        $scope.open = function(contact) {

            var modalInstance = $modal.open({
                animation: true,
                templateUrl: '/views/templates/modal.html',
                controller: 'ModalCtrl',
                resolve: {
                    contact: function() {
                        return contact;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });

        };


    });
