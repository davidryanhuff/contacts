'use strict';

/**
 * @ngdoc function
 * @name contactformApp.controller:ModalCtrl
 * @description
 * # ModalCtrl
 * Controller of the contactformApp
 */
angular.module('contactformApp')
    .controller('ModalCtrl', function($scope, $modalInstance, Ref, $firebaseArray, contact) {

        var contacts = $firebaseArray(Ref);
        $scope.contact = contact;

        if (contact !== undefined) {
            $scope.editMode = true;
        } else {
            $scope.editMode = false;
        }

        $scope.saveContact = function() {
            contacts.$add({
                    firstname: $scope.contact.firstname,
                    lastname: $scope.contact.lastname,
                    email: $scope.contact.email,
                    phone: $scope.contact.phone,
                    address: $scope.contact.address || null,
                    city: $scope.contact.city || null,
                    state: $scope.contact.state || null,
                    country: $scope.contact.country || null,
                    zip: $scope.contact.zip || null
                })
                .then(function() {
                    $modalInstance.close();
                });
        };

        $scope.editContact = function() {
            console.log('contact.$id = ', contact.$id);
            var contactToEdit = contacts.$getRecord(contact.$id);
                contactToEdit.firstname = $scope.contact.firstname;
                contactToEdit.lastname = $scope.contact.lastname;
                contactToEdit.email = $scope.contact.email;
                contactToEdit.phone = $scope.contact.phone;
                contactToEdit.address = $scope.contact.address;
                contactToEdit.city = $scope.contact.city;
                contactToEdit.state = $scope.contact.state;
                contactToEdit.country = $scope.contact.country;
                contactToEdit.zip = $scope.contact.zip;
                console.log('contact to edit', contactToEdit);

                contacts.$save(contactToEdit).then(function() {
                console.log('contact to edit in save', contactToEdit);

                    $modalInstance.close($scope.contact);
                });


        };

        $scope.close = function() {
            $modalInstance.close();
        };

    });
