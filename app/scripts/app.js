'use strict';

/**
 * @ngdoc overview
 * @name contactformApp
 * @description
 * # contactformApp
 *
 * Main module of the application.
 */
angular.module('contactformApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'firebase.ref',
    'ui.bootstrap',
    'angularValidator',
    'countrySelect'
  ]);
